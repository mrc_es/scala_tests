package com

object UnifiedTypes {

  def getList() {

    val lista: List[Any] = List(
      "uno",
      2,
      "Tres"
    )
    lista.foreach(element => println(element))
    lista.reverse.foreach(element => println(element))
  }

  def intStuff() {

    val x: Int = 10
    println(x.toFloat)
    println(x.toString)
  }

}
