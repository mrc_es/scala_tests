package com

object ClassTests {

  class Class1(x: Int, y: Int) {
    def metodo1(): Int = {
      x + y
    }
  }

  class Class2(var x: Int) {
    def sum1(): Class2 = {
      x = x + 1
      this
    }
    val show: () => Unit = () => println(this.x)
  }

  class PrivateClass {

    private var _x = 0
    private var _y = 0

    def x = _x
    def y = _y

    def x_= (new_x: Int): Unit = {
      if (new_x > 100)
        this._x = new_x
      else
        println("Debe ser mayor a 100")
    }

    def y_= (new_y: Int): Unit = {
      if (new_y > 100)
        this._y = new_y
      else
        println("Debe ser mayor a 100")
    }

  }

  class Named {

    def named(named1: Int, named2: String): Unit = {
      println(s"named1 = $named1")
      println(s"named2 = $named2")
    }
  }
  class Dog(name: String, age: Int) {
    var _food = ""
    var _name = name

    def likes(food: String): Unit = {
      this._food = food
    }
    def show() {
      println(s"${this._name}:${this.age}:${this._food}")
    }

    def walksWith(dogo: Dog): Unit = {
      print(s"${this._name} walks with ${dogo._name}\n")
    }

    def &(dogo: Dog): Unit = {
      print(s"${this._name} walks with ${dogo._name}\n")
    }

  }
}
