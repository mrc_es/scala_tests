package com

import com.UnifiedTypes.{getList, intStuff}
import com.ClassTests.{Class1, Class2, PrivateClass, Named, Dog}

object App {
  def main(args: Array[String]): Unit = {
    //getList()
    //intStuff()
    val clase1 = new Class1(2,3)
    println(clase1.metodo1())

    val clase2 = new Class2(10)
    clase2.sum1.sum1.sum1.show()

    val pc = new PrivateClass

    pc.x = 10
    pc.y = 200

    println(pc.y)

    val nm = new Named()
    nm.named(named1=10, named2="Hola")

    val choco = new Dog(name="Choco", age=10)

    choco likes "Salchichas"

    choco.show()

    val fido = new Dog(name="Fido", age=5)

    fido walksWith choco
    choco & fido

  }
}
